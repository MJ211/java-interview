![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Java interview knowledge

A repository for gitlab pages.

Link: https://mj211.gitlab.io/java-interview/

## Changing commit author

To change author of a last commit:

1. Execute :
```
git rebase -i HEAD~1
```
2. Change `pick` to `edit`
3. Run:
```
git commit --amend --author="MJ211 <jedrzejkiewicz.michal@gmail.com>" 
git rebase --continue 
git push
```

In extreme situations:
1. Remove a `master` branch from protected branches in `Settings -> Repository`
2. Change variables and execute:
```
git filter-branch --env-filter '
WRONG_EMAIL="wrong@example.com"
NEW_NAME="New Name Value"
NEW_EMAIL="correct@example.com"

if [ "$GIT_COMMITTER_EMAIL" = "$WRONG_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$NEW_NAME"
    export GIT_COMMITTER_EMAIL="$NEW_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$WRONG_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$NEW_NAME"
    export GIT_AUTHOR_EMAIL="$NEW_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
git push -f
```
3. Move the `master` branch back to protected branches
