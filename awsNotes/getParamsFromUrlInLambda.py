import json

def lambda_handler(event, context):
    # TODO implement
    firstNumber = 0
    secondNumber = 0
    print (event['queryStringParameters'])
    if event['queryStringParameters']:
        firstNumber = int(event['queryStringParameters']['first'])
        secondNumber = int(event['queryStringParameters']['second'])

    return {
        'statusCode': 200,
        'body': json.dumps('Sum: ' + str(firstNumber + secondNumber))
    }
    
