import json
from datetime import datetime

def clock(event, context):
    print("Clock function works!")
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")

    return {
        "statusCode": 200,
        "body": json.dumps("Current Time = " + current_time),
    }