x = 11
def outer():
    x = 22
    def inner():
        x *= 2
        def inner():
            global x
            x *= 3
        inner()
    
    inner()
    print("outer:", x)

outer()
print("outer:", x)